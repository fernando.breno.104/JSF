package beans;

import java.io.Serializable;
import java.util.List;

import dbUtil.TarefaDao;
import entity.Tarefa;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.bean.ManagedBean;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Query;

@Named("tarefaBean")
@ManagedBean
@SessionScoped
public class TarefaBean implements Serializable{

    private Tarefa tarefa;
    private List<Tarefa> tarefas;


    public TarefaBean() {
        tarefa = new Tarefa();
    }

    public Tarefa getTarefa() {
        return tarefa;
    }

    public void setTarefa(Tarefa tarefa) {
        this.tarefa = tarefa;
    }
    public List<Tarefa> getTarefas() {
        return tarefas;
    }
    public void setTarefas(List<Tarefa> tarefas) {
        this.tarefas = tarefas;
    }

    public void adicionar() {
        EntityManager em = TarefaDao.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        tarefa = em.merge(tarefa);
        em.persist(tarefa);
        transaction.commit();
        em.close();
        tarefa = new Tarefa();
        atualizar();

    }

    public void remover() {
        EntityManager em = TarefaDao.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        tarefa = em.merge(tarefa);
        em.remove(tarefa);
        transaction.commit();
        em.close();
        tarefa = new Tarefa();
        atualizar();
    }

    public void atualizar() {
        EntityManager em = TarefaDao.getEntityManager();
        String sqlQuery = "select * from tarefa";
        Query query = em.createNativeQuery(sqlQuery, Tarefa.class);
        this.tarefas = query.getResultList();
        em.close();
    }

    public void mensagem() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("OK", "OK"));
        System.out.println("mensagem inserida");
    }



}
