package dbUtil;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.PersistenceContext;

public class TarefaDao {
    @PersistenceContext
    private static final EntityManagerFactory EM_FACTORY =
            Persistence.createEntityManagerFactory("default");

    public static EntityManager getEntityManager() {
        return EM_FACTORY.createEntityManager();
    }
}
